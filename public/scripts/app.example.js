class App {
    constructor() {
        this.clearButton = document.getElementById("clear-btn");
        this.loadButton = document.getElementById("load-btn");
        this.carContainerElement = document.getElementById("cars-container");
    }

    async init() {
        await this.load();

        // Register click listener
        //  this.clearButton.onclick = this.clear;
        // this.loadButton.onclick = this.run;
    }

    run = () => {
        Car.list.forEach((car) => {
            const node = document.createElement("div");
            node.className = 'col-ms-12 col-lg-4';
            node.innerHTML = car.render();
            this.carContainerElement.appendChild(node);
        });
    };

    async load() {
        this.clear();
        const passenger = document.getElementById('passenger');
        const date = document.getElementById('date');
        const time = document.getElementById('time');

        const passengerSeat = passenger.value;
        const dateRent = date.value;
        const timeRent = time.value;
        console.log(passengerSeat);
        console.log(dateRent);
        console.log(timeRent);

        // let inputDateTime = Date.getTime(dateRent +'T'+ timeRent+'Z');
        let inputDateTime = new Date(dateRent + 'T' + timeRent + 'Z');
        let calculateDateTime = inputDateTime.getTime();
        console.log(calculateDateTime);

        const cars = await Binar.listCars();
        // console.log(cars.length)
        const availableCar = cars.filter((car) => {
            return car.capacity >= passengerSeat && car.available === true && car.availableAt.getTime() > calculateDateTime;
        })
        Car.init(availableCar);
    }

    clear = () => {
        let child = this.carContainerElement.firstElementChild;

        while (child) {
            child.remove();
            child = this.carContainerElement.firstElementChild;
        }
    };
}