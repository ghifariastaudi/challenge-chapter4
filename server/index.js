const express = require('express');
const path = require('path');

const app = express();

app.use(express.static('public'));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '/index.html'));
});

app.get('/car', function(req, res) {
    res.sendFile(path.join(__dirname, '../public/car.html'));
});

app.listen(3000, '0.0.0.0', () => {
    console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", 3000);
});
/**
 * Impor HTTP Standar Library dari Node.js
 * */
// const http = require('http');
// const { PORT = 3000 } = process.env; // Set port

// const fs = require('fs');
// const path = require('path');
// const PUBLIC_DIRECTORY = path.join(__dirname, '../public');

// //  handle RenderHTML
// function renderHTML(htmlFileName) {
//     const htmlFilePath = path.join(PUBLIC_DIRECTORY, htmlFileName)
//     return fs.readFileSync(htmlFilePath, 'utf-8')
// }


// function onRequest(req, res) {
//     switch (req.url) {
//         case "/":
//             res.writeHead(200, { 'Content-Type': 'text/html' })
//             res.end(renderHTML('index.html'))
//             return;
//         case "/car":
//             res.writeHead(200, { 'Content-Type': 'text/html' })
//             res.end(renderHTML('car.html'))
//             return;
//         default:

//             if ((req.url.indexOf('.css') != -1) || (req.url.indexOf('.png')) != -1 || (req.url.indexOf('.jpg')) != -1) {
//                 const cssFilePath = path.join(PUBLIC_DIRECTORY, `/${req.url}`)

//                 fs.readFile(cssFilePath, function(err, data) {
//                     console.log("-->", req.url)
//                     res.writeHead(200, { 'Content-Type': ['text/css', 'image/png', 'image/jpg'] })

//                     res.write(data);

//                     res.end()
//                 })
//             } else {
//                 res.writeHead(404)
//                 res.end("File Not Found")
//             }


//     }
// }

// const server = http.createServer(onRequest);

// // Jalankan server
// server.listen(PORT, 'localhost', () => {
//     console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
// })